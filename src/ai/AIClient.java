package ai;

import ai.Global;
import java.io.*;
import java.net.*;
import javax.swing.*;
import java.awt.*;
import kalaha.*;
import static java.lang.Math.max;
import static java.lang.Math.min;

/**
 * This is the main class for your Kalaha AI bot. Currently it only makes a
 * random, valid move each turn.
 *
 * @author Johan Hagelbäck
 */
public class AIClient implements Runnable {

    private int player;
    private JTextArea text;

    private PrintWriter out;
    private BufferedReader in;
    private Thread thr;
    private Socket socket;
    private boolean running;
    private boolean connected;
    public int startMove = 0;


    /**
     * Creates a new client.
     */
    public AIClient() {
        player = -1;
        connected = false;

        //This is some necessary client stuff. You don't need
        //to change anything here.
        initGUI();

        try {
            addText("Connecting to localhost:" + KalahaMain.port);
            socket = new Socket("localhost", KalahaMain.port);
            out = new PrintWriter(socket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
            addText("Done");
            connected = true;
        } catch (Exception ex) {
            addText("Unable to connect to server");
            return;
        }
    }

    /**
     * Starts the client thread.
     */
    public void start() {
        //Don't change this
        if (connected) {
            thr = new Thread(this);
            thr.start();
        }
    }

    /**
     * Creates the GUI.
     */
    private void initGUI() {
        //Client GUI stuff. You don't need to change this.
        JFrame frame = new JFrame("My AI Client");
        frame.setLocation(Global.getClientXpos(), 445);
        frame.setSize(new Dimension(420, 250));
        frame.getContentPane().setLayout(new FlowLayout());

        text = new JTextArea();
        JScrollPane pane = new JScrollPane(text);
        pane.setPreferredSize(new Dimension(400, 210));

        frame.getContentPane().add(pane);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        frame.setVisible(true);
    }

    /**
     * Adds a text string to the GUI textarea.
     *
     * @param txt The text to add
     */
    public void addText(String txt) {
        //Don't change this
        text.append(txt + "\n");
        text.setCaretPosition(text.getDocument().getLength());
    }

    /**
     * Thread for server communication. Checks when it is this client's turn to
     * make a move.
     */
    public void run() {
        String reply;
        running = true;

        try {
            while (running) {
                //Checks which player you are. No need to change this.
                if (player == -1) {
                    out.println(Commands.HELLO);
                    reply = in.readLine();

                    String tokens[] = reply.split(" ");
                    player = Integer.parseInt(tokens[1]);

                    addText("I am player " + player);
                }

                //Check if game has ended. No need to change this.
                out.println(Commands.WINNER);
                reply = in.readLine();
                if (reply.equals("1") || reply.equals("2")) {
                    int w = Integer.parseInt(reply);
                    if (w == player) {
                        addText("I won!");
                    } else {
                        addText("I lost...");
                    }
                    running = false;
                }
                if (reply.equals("0")) {
                    addText("Even game!");
                    running = false;
                }

                //Check if it is my turn. If so, do a move
                out.println(Commands.NEXT_PLAYER);
                reply = in.readLine();
                if (!reply.equals(Errors.GAME_NOT_FULL) && running) {
                    int nextPlayer = Integer.parseInt(reply);

                    if (nextPlayer == player) {
                        out.println(Commands.BOARD);
                        String currentBoardStr = in.readLine();
                        boolean validMove = false;
                        while (!validMove) {
                            long startT = System.currentTimeMillis();
                            //This is the call to the function for making a move.
                            //You only need to change the contents in the getMove()
                            //function.
                            GameState currentBoard = new GameState(currentBoardStr);
                            int cMove = getMove(currentBoard);

                            //Timer stuff
                            long tot = System.currentTimeMillis() - startT;
                            double e = (double) tot / (double) 1000;

                            out.println(Commands.MOVE + " " + cMove + " " + player);
                            reply = in.readLine();
                            if (!reply.startsWith("ERROR")) {
                                validMove = true;
                                addText("Made move " + cMove + " in " + e + " secs");
                            }
                        }
                    }
                }

                //Wait
                Thread.sleep(100);
            }
        } catch (Exception ex) {
            running = false;
        }

        try {
            socket.close();
            addText("Disconnected from server");
        } catch (Exception ex) {
            addText("Error closing connection: " + ex.getMessage());
        }
    }

    /**
     * This is the method that makes a move each time it is your turn. Here you
     * need to change the call to the random method to your Minimax search.
     *
     * @param currentBoard The current board state
     * @return Move to make (1-6)
     */
    public int getMove(GameState currentBoard) {
        int myMove = 1;
        //Start AI on a new thread running for 5 seconds before terminating
        TimedAI newAI = new TimedAI(currentBoard);
        Thread t = new Thread(newAI);
        t.start();
        synchronized (t) {
            try {   
                t.wait(4980);
                t.interrupt();
            } catch (Exception e) {

            }
        }
        myMove = newAI.currentBestMove;
        t = null;
        return myMove;
    }


    class TimedAI implements Runnable{

        public int currentBestMove = 0;
        int currentBestValue = Integer.MIN_VALUE;
        int alpha;
        int beta;
        //Start maxlevel for iterative deepening
        int startLevel = 5;
        GameState gameState;
        boolean treeFinished;

        public TimedAI(GameState currentBoard) {
            gameState = currentBoard;
        }

        @Override
        public void run() {
            synchronized (this) {
                startLevel = 5;
                while (true) {
                    treeFinished = false;
                    searchTree(gameState, 0, startLevel++);
                    if (treeFinished) {
                        break;
                    }
                }
            }
        }
        //Creates the initial branches for the search tree
        public int searchTree(GameState currentState, int currentLevel, int maxLevel) {
            alpha = Integer.MIN_VALUE;
            beta = Integer.MAX_VALUE;
            int value = Integer.MIN_VALUE;

            for (int i = 1; i <= 6; i++) {

                GameState tempGameBoard = currentState.clone();
                if (tempGameBoard.makeMove(i)) {
                    int childValue = searchTreeRec(tempGameBoard, currentLevel + 1, maxLevel);

                    if (childValue > value) {
                        value = childValue;
                        if (value > currentBestValue) {
                            currentBestMove = i;
                            currentBestValue = childValue;
                        }
                    }
                }
            }
            return 0;
        }

        //Recursive minimax search
        public int searchTreeRec(GameState currentState, int currentLevel, int maxLevel) {
            boolean maxTurn = currentState.getNextPlayer() == player;
            //If final state on our turn is reached stop execution
            if (currentState.gameEnded()) {
                if(maxTurn)
                    treeFinished = true;
                return EvaluateState(currentState);
            } else if (currentLevel >= maxLevel) {
                return EvaluateState(currentState);
            }

            int value = maxTurn ? Integer.MIN_VALUE : Integer.MAX_VALUE;

            for (int i = 1; i <= 6; i++) {
                GameState tempGameBoard = currentState.clone();
                if (maxTurn) {
                    //Ignore(prune) if value > alpha on max turn
                    if (tempGameBoard.makeMove(i) && !(value > alpha)) {
                        int childValue = searchTreeRec(tempGameBoard, currentLevel + 1, maxLevel);
                        value = max(childValue, value);
                        if (value > alpha) {
                            alpha = value;
                        }
                    } else {
                    }
                    //Ignore(prune) if value < beta on min turn
                } else if (tempGameBoard.makeMove(i) && !(value < beta)) {
                    int childValue = searchTreeRec(tempGameBoard, currentLevel + 1, maxLevel);
                    value = min(childValue, value);
                    if (value < beta) {
                        beta = value;
                    }
                } else {
                }

            }
            return value;
        }
    }

    public int EvaluateState(GameState currentState) {
        return currentState.getScore(player) - currentState.getScore(player == 1 ? 2 : 1);
    }

    /**
     * Returns a random ambo number (1-6) used when making a random move.
     *
     * @return Random ambo number
     */
    public int getRandom() {
        return 1 + (int) (Math.random() * 6);
    }
}
